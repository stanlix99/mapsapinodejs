const puppeteer = require('puppeteer');
const express = require('express');
const app = express()


function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time)
    });
}
app.get('/', async (req, res) => {
    res.send("hello world")
})


    app.get('/getPlaces', async (req, res) => {
        try{
            res.setHeader('Content-Type', 'application/json');
            console.time('doSomething')
            const browser = await puppeteer.launch({ defaultViewport: null ,args: ['--no-sandbox']})
            const page = await browser.newPage()
            await page.setViewport({ width: 1920, height: 2000 });
            await page.goto('https://www.google.com/maps/',{ waitUntil: 'domcontentloaded'})
            page.setDefaultTimeout(10000);
    
            //click aceptar
    
            delay(2000)
            // const elements = await page.$x('/html/body/c-wiz/div/div/div/div[2]/div[1]/div[4]/form/div/div/button')
            // await elements[0].evaluate(ele => ele.click(),elements[0])

            const linkHandlers = await page.$x('/html/body/c-wiz/div/div/div/div[2]/div[1]/div[4]/form/div/div/button');
            await page.screenshot({ path: 'error.png' });
            if (linkHandlers.length > 0) {
            await linkHandlers[0].click();
            } else {
            throw new Error("Link not found");
            }

            
    
    
            // wait for element defined by XPath appear in page 
            await page.waitForXPath("/html/body/div[3]/div[9]/div[3]/div[1]/div[1]/div[1]/div[2]/form/div/div[3]/div/input[1]");
            // introduce query en el buscador de maps
            await page.focus('input[name=q]')
    
            // QUERY INPUT FROM DEVICE*********************************************************************************************
    
            await page.keyboard.type('picnic places lloret de mar')
    
            // QUERY INPUT FROM DEVICE*********************************************************************************************
            await page.keyboard.press('Enter');
    
    
            // add elements to array
            let arrayResults = []
            for (var index = 3; index < 50; index = index + 2) {
                let element = `/html/body/div[3]/div[9]/div[8]/div/div[1]/div/div/div[2]/div[1]/div[${index}]/div/a`
                let errorLoop;
    
                await page.waitForXPath(element).catch(() => {
                    errorLoop = true;
                    return
                })
                if (errorLoop == true) {
                    break
                }
                let elementHandle = await page.$x(element.toString());
                // await page.screenshot({ path: 'error.png' });
                let elementTitle = await page.evaluate(el => el.getAttribute('aria-label'), elementHandle[0]);
                let elementLink = await page.evaluate(el => el.getAttribute('href'), elementHandle[0]);
                await page.waitForSelector(`div.section-scrollbox:nth-child(1) > div:nth-child(${index}) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > img:nth-child(1)`)
                let elementPhotoContent = await page.$eval(`div.section-scrollbox:nth-child(1) > div:nth-child(${index}) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > img:nth-child(1)`, ele => ele.getAttribute('src'));
    
                //get latitude longitude
                var strig = elementLink
                var result = strig.split(/(!8m2!3d)/)
                var result2 = result[2].split(/(!4d)/)
                const latitude = result2[0]
    
                var result3 = result2[2].split(/(\?auth)/)
                const longitude = result3[0]
    
    
                let elementObject = {"title": elementTitle,"photo": elementPhotoContent,"mapsLink":elementLink,"coordinates":{"latitude":latitude,"longitude":longitude}}
                arrayResults.push(elementObject)
            }
            
    
    
    
            await browser.close();
            console.timeEnd('doSomething')
            
            
            res.send(JSON.stringify(arrayResults))
        }catch(e){
            console.log(e)
        }

    })





    // app.post('/getPlaces', async (req, res) => {
    //     res.setHeader('Content-Type', 'application/json');
    //     console.time('doSomething')
    //     const browser = await puppeteer.launch({ defaultViewport: null });
    //     const page = await browser.newPage()
    //     await page.setViewport({ width: 1920, height: 2000 });
    //     await page.goto('https://www.google.com/maps/')
    //     page.setDefaultTimeout(10000);

    //     //click aceptar

    //     const elements = await page.$x('/html/body/c-wiz/div/div/div/div[2]/div[1]/div[4]/form/div/div/button')
    //     await elements[0].click()
    //     // await page.screenshot({ path: 'maps.png' });

    //     // wait for element defined by XPath appear in page 
    //     await page.waitForXPath("/html/body/div[3]/div[9]/div[3]/div[1]/div[1]/div[1]/div[2]/form/div/div[3]/div/input[1]");
    //     // introduce query en el buscador de maps
    //     await page.focus('input[name=q]')

    //     // QUERY INPUT FROM DEVICE*********************************************************************************************

    //     await page.keyboard.type(`picnic places ${req.body.location})`)

    //     // QUERY INPUT FROM DEVICE*********************************************************************************************
    //     await page.keyboard.press('Enter');


    //     // add elements to array
    //     let arrayResults = []
    //     for (var index = 3; index < 50; index = index + 2) {
    //         let element = `/html/body/div[3]/div[9]/div[8]/div/div[1]/div/div/div[2]/div[1]/div[${index}]/div/a`
    //         let errorLoop;

    //         await page.waitForXPath(element).catch(() => {
    //             errorLoop = true;
    //             return
    //         })
    //         if (errorLoop == true) {
    //             break
    //         }
    //         let elementHandle = await page.$x(element.toString());
    //         // await page.screenshot({ path: 'error.png' });
    //         let elementTitle = await page.evaluate(el => el.getAttribute('aria-label'), elementHandle[0]);
    //         let elementLink = await page.evaluate(el => el.getAttribute('href'), elementHandle[0]);
    //         await page.waitForSelector(`div.section-scrollbox:nth-child(1) > div:nth-child(${index}) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > img:nth-child(1)`)
    //         let elementPhotoContent = await page.$eval(`div.section-scrollbox:nth-child(1) > div:nth-child(${index}) > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > img:nth-child(1)`, ele => ele.getAttribute('src'));

    //         //get latitude longitude
    //         var strig = elementLink
    //         var result = strig.split(/(!8m2!3d)/)
    //         var result2 = result[2].split(/(!4d)/)
    //         const latitude = result2[0]

    //         var result3 = result2[2].split(/(\?auth)/)
    //         const longitude = result3[0]


    //         let elementObject = {"title": elementTitle,"photo": elementPhotoContent,"mapsLink":elementLink,"coordinates":{"latitude":latitude,"longitude":longitude}}
    //         arrayResults.push(elementObject)
    //     }
        



    //     await browser.close();
    //     console.timeEnd('doSomething')
        
        
    //     res.send(JSON.stringify(arrayResults))
    // })


app.listen(process.env.PORT || 3000)
